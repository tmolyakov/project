<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LuckyController
 * @package App\Controller
 *
 * @Route("/lucky", name="lucky_")
 */
class LuckyController extends AbstractController
{
    /**
     * @Route("/number", name="number")
     *
     * @return Response
     * @throws \Exception
     */
    public function number()
    {
        $number = random_int(0, 100);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }

    /**
     * @Route("/number/{number}", name="my", requirements={"number"="\d+"}, defaults={"number": 1})
     *
     * @param $number
     * @return Response
     */
    public function my_number(int $number)
    {
        return $this->render("lucky/number.html.twig", [
            'number' => $number,
        ]);
    }
}