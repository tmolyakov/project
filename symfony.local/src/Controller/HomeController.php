<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }
    /**
     * @Route("/admin", name="admin")
     * @return Response
     */
    public function admin()
    {
        return $this->render('admin.html.twig');
    }

    /**
     * @Route("/test", name="test")
     * @return Response
     */
    public function test()
    {
        return $this->render('base.html.twig');
    }
}